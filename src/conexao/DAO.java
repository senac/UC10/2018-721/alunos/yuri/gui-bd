/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexao;

import java.util.List;

/**
 *
 * @author sala304b
 * @param <T>
 */
public interface DAO<T> {
    
    void inserir(T objeto);
    void atualizar(T objeto);
    void delete(int id);
    List<T> listarTodos();
    T buscarPorId(int id);
    
}
