/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexao;

import Agenda.Pessoa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PessoaDAO implements DAO<Pessoa> {

    @Override
    public void inserir(Pessoa pessoa) {
        Connection connection = null;

        try {
            connection = Conexao.getConnection();
            String query = "INSERT INTO PESSOA (NOME , TELEFONE ) VALUES (? , ?)";
            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, pessoa.getNome());
            ps.setString(2, pessoa.getTelefone());

            int linha = ps.executeUpdate();
            if (linha > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                rs.first();
                int id = rs.getInt(1);
                pessoa.setId(id);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }


    @Override
    public void atualizar(Pessoa pessoa) {
        Connection connection = null;
        try {
            String query = "UPDATE PESSOA SET NOME = ? ,TELEFONE = ? WHERE ID = ? ";
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, pessoa.getNome());
            ps.setString(2, pessoa.getTelefone());
            ps.setInt(3, pessoa.getId());
            
            ps.executeUpdate();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void delete(int id) {
        Connection connection = null;
        try {
            //abrir conexao
            connection = Conexao.getConnection();
            //definir query 
            String query = "DELETE FROM PESSOA WHERE ID = ?";
            //criar objeto que vai executar a query 
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1,id);
            //excutar query 
            ps.executeUpdate();
            System.out.println("deletando .....");

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                //fechar conexao
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public List<Pessoa> listarTodos() {

        Connection connection = null;
        List<Pessoa> lista = new ArrayList<>();
        try {

            connection = Conexao.getConnection();
            String query = "SELECT * FROM PESSOA ";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                int id = rs.getInt("id");
                String nome = rs.getString("nome");
                String telefone = rs.getString("telefone");
                Pessoa pessoa = new Pessoa(id, nome, telefone);
                lista.add(pessoa);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return lista;
    }

    @Override
    public Pessoa buscarPorId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Pessoa> buscarPorNome(String nomeBuscado) {
        
        Connection connection = null;
        List<Pessoa> listar = new ArrayList<>();
        try {
            //abrir conexao
            connection = Conexao.getConnection();
            //definir query 
            String query = "SELECT * FROM PESSOA WHERE NOME LIKE = ? ";
            //criar objeto que vai executar a query 
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1,nomeBuscado + "%");

            //excutar query 
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String nome = rs.getString("nome");
                String telefone = rs.getString("telefone");
                Pessoa pessoa = new Pessoa(id , nome , telefone);
                listar.add(pessoa);
                
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                //fechar conexao
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        return listar;

    }
    

    public int getQuantidadeContatos() {
        Connection connection = null;
        int quantidade = 0 ; 
        try {

            connection = Conexao.getConnection();
            String query = "SELECT count(1) as quantidade FROM PESSOA";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            rs.first();
            quantidade = rs.getInt("quantidade");
           
                
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        
        return quantidade ; 
    }

    
    

}