/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author sala304b
 */
public class Conexao {
    
    private static final String URL = "jdbc:mysql://localhost:3306/SCHEDULE_BD";
    private static final String USER = "root";
    private static final String PASSWORD = "123456";
    private static final String DRIVER = "com.mysql.jdbc.Driver";

    public static Connection getConnection(){
        Connection connection = null;
        
        try{
            //carregando o driver do banco
            Class.forName(DRIVER);
            //Abrir conexao com o banco
            connection = (Connection) DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println("Conectou!");
        
        }catch (ClassNotFoundException ex){
            System.out.println("Erro ao carregar o Driver do banco");
            ex.printStackTrace();
        
        }catch(SQLException ex){
            ex.printStackTrace();
            System.out.println("Erro ao conectar ao banco");
        }
        
        return connection;
    }
    
   
    public static void main(String[] args) {
        getConnection();
    }
    
}
